const getRandomColor = () => {
  const letters = "0123456789ABCDEF";
  let color = "#";
  for (let i = 0; i < 6; i++) {
    color += letters[Math.floor(Math.random() * 16)];
  }
  return color;
};

const body = document.getElementsByTagName("body")[0];

document.getElementById("btn").addEventListener("click", () => {
  body.style.background = getRandomColor();
});
